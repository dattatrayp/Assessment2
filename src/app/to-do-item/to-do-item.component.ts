import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-to-do-item',
  templateUrl: './to-do-item.component.html',
  styleUrls: ['./to-do-item.component.css']
})
export class ToDoItemComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  todoList = [];
  addTask(taskname, status, priority) {
    var task = {
      name: taskname,
      status: status,
      priority: priority
    };

    if (task) {
      this.todoList.push(task);
    }
  }
}
