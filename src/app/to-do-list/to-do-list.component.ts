import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent implements OnInit {

  todoList = [
    { name: 'hrt training', status: 'done', priority: 'high' },
    { name: 'pit training', status: 'not started', priority: 'low' },
    { name: 'angular training', status: 'in progress', priority: 'high' },

  ];
  constructor() { }

  ngOnInit() {
  }

}
